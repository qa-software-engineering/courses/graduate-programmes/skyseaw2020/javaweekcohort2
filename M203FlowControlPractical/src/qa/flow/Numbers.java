/* Using loops */
package qa.flow;

public class Numbers
{
	public static void main(String[] args)
	{

		//
		// ToDo:
		//
		// Add your code here to print out table
		// of 2 to the power n where n < 64
		//

		long n = 1;
		long result = 0;
		long max10DigitLong = 9999999999L;

		while (n < 65) {

			if (n == 1) {
				result = 2 * n;
			}
			else {
				result *= 2;
			}
			if (result > max10DigitLong) break;
			System.out.printf("%d\t%d%n", n, result);
			n++;

		}

		// Start as 'while' loop then switch to 'for' loop

		for(int i = 1; i < 65; i++) {
			if(i == 1) {
				result = 2 * i;
			}
			else {
				result *= 2;
			}
			if (result > max10DigitLong) break;
			String nSpacer = "";
			if(i < 10) nSpacer = " ";

			String resultSpacer = "";
			long resultChecker = result;

			for(int j = 0; j < 10; j++) {
				if(resultChecker < max10DigitLong) {
					resultSpacer += " ";
					resultChecker *= 10;
				}
			}

			System.out.printf(nSpacer + "%d\t" + resultSpacer + "%d%n", i, result);

			if (result > max10DigitLong) break;
		}

		// Call your variables 'i' and 'result'

	




      }

}

