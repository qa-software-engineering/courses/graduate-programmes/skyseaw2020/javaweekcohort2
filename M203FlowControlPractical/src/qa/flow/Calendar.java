/* Using the switch statement */
package qa.flow;

public class Calendar
{
	public static void main(String[] args)
	{
        byte currentMonth = 3;     // a number between 0 and 11
		boolean leapYear = false;  // not a leap year
		byte numberOfDaysInCurrentMonth = 0;


		//
		// ToDo:
		//
		// Add your code here to work out number of days in current month
		//

		switch(currentMonth) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				numberOfDaysInCurrentMonth = 31;
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				numberOfDaysInCurrentMonth = 30;
				break;
			case 2:
				if(leapYear) numberOfDaysInCurrentMonth = 29;
				if(!leapYear) numberOfDaysInCurrentMonth = 28;
				break;
			default:
				break;
		}


		//
		// ToDo:
		//
		// Now display the result
		//


		System.out.println("The current month is: " + currentMonth);
		System.out.println("There are " + numberOfDaysInCurrentMonth + " days in this month");

    }

}

