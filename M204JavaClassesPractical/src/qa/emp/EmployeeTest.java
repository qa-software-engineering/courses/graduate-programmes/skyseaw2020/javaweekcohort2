package qa.emp;

public class EmployeeTest {
    public static void main(String[] args) {

//        Employee employee1 = new Employee("Smith", 21);
//        Employee employee2 = new Employee("Jones", 42);

//        Employee employee1 = new Employee("John","Smith", 21);
//        Employee employee2 = new Employee("Jane", "Jones", 42);
//
//        System.out.println("Employee details: " + employee1.getLastName() + "\t\t" + employee1.getAge());
//        System.out.println("Employee details: " + employee2.getLastName() + "\t\t" + employee2.getAge());
//
//
//        for (int i = 0; i < 50; i++) {
//            employee1.incAge();
//
//            System.out.println("Employee details: " + employee1.getLastName() + "\t\t" + employee1.getAge());
//            System.out.println("Employee details: " + employee2.getLastName() + "\t\t" + employee2.getAge());
//        }

//        Employee employee1 = new Employee("John","Smith", 21);
//        Employee employee2 = new Employee("Jane", "Jones", 42);
//
//        System.out.println("Employee details: " + employee1.getName() + "\t\t" + employee1.getAge());
//        System.out.println("Employee details: " + employee2.getName() + "\t\t" + employee2.getAge());
//
//
//        for (int i = 0; i < 50; i++) {
//            employee1.incAge();
//
//            System.out.println("Employee details: " + employee1.getName() + "\t\t" + employee1.getAge());
//            System.out.println("Employee details: " + employee2.getName() + "\t\t" + employee2.getAge());
//        }

//        Employee employee1 = new Employee("John","Smith", 21);
//        Employee employee2 = new Employee("Jane", "Jones", 42);
//
//        System.out.println("Employee details: " + employee1.getName() + "\t\t" + employee1.getAge());
//        System.out.println("Employee details: " + employee2.getName() + "\t\t" + employee2.getAge());
//
//        Employee.setRetirementAge(75);
//
//        for (int i = 0; i < 60; i++) {
//            employee1.incAge();
//
//            System.out.println("Employee details: " + employee1.getName() + "\t\t" + employee1.getAge());
//        }
//
//        Employee.setRetirementAge(68);
//
//        for (int i = 0; i < 60; i++) {
//            employee2.incAge();
//
//            System.out.println("Employee details: " + employee2.getName() + "\t\t" + employee2.getAge());
//        }


        Employee employee1 = new Employee("John","Smith", 21);
        Employee employee2 = new Employee("Jane", "Jones", 42);
        Manager manager1 = new Manager("Michelle", "Management", 33);

//        System.out.println("Employee details: " + employee1.getName() + "\t\t" + employee1.getAge());
//        System.out.println("Employee details: " + employee2.getName() + "\t\t" + employee2.getAge());
//        System.out.println("Employee details: " + manager1.getName() + "\t\t" + manager1.getAge());

        Employee[] employees = {employee1, employee2, manager1};

        for(int i = 0, j = employees.length; i < j; i++) {
            System.out.println("Employee details: " + employees[i].getName() + "\t\t" + employees[i].getAge());
        }

        manager1.addEmployee(employee1);
        manager1.addEmployee(employee2);

        for(int i = 0, j = employees.length; i < j; i++) {
            System.out.println(employees[i].getDetails());
        }
    }
}
