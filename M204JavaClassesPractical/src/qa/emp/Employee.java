package qa.emp;

public class Employee {

    private String firstName;
    private String lastName;
    private int age;

    public static int retirementAge = 65;

    public Employee(String lastName, int age) {
        this.lastName = lastName;
        this.age = age;
    }

    public Employee(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getName() {
        return firstName + " " + lastName;
    }

    public int getAge() {
        return age;
    }

    public void incAge() {
        if (this.age <= retirementAge) {
            this.age++;
        }
    }

    public static void setRetirementAge(int retirementAge) {
        Employee.retirementAge = retirementAge;
    }

    public String getDetails() {
        return getName() + "\t" + age + ";";
    }
}
