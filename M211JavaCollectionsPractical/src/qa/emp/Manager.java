package qa.emp;

public class Manager extends Employee {

    private Employee[] manages;
    private int lastIndexUsed = 0;

    public Manager(String lastName, int age) {
        super(lastName, age);
    }

    public Manager(String firstName, String lastName, int age) {
        super(firstName, lastName, age);
        manages = new Employee[100];

    }

    public void addEmployee(Employee employee) {
        if(employee instanceof Manager) return;     // Stops managers being added to the manages array
        manages[lastIndexUsed++] = employee;
    }

    public String getEmployeeNames() {
        String names = "";
        for(int i = 0; i < lastIndexUsed; i++) {
            names += manages[i].getName();
            if (i != lastIndexUsed -1) {
                names += ", ";
            }
        }
        return names;
    }

    @Override
    public String getDetails() {
        return super.getDetails() + "\t Manages: " + getEmployeeNames();
    }
}
