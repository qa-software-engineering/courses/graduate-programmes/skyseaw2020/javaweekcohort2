package qa.emp;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmployeeTest {
    public static String createRandomName() {
        String name = "";
        int randomNameLength = (int) ((Math.random() * 10));
        randomNameLength = randomNameLength < 2 ? 6 : randomNameLength;
        for(int j = 0; j < randomNameLength; j++) {
            char c = (char) ((Math.random() * 26) + 65);
            if ( j == 0 ) {
                name += c;
                name = name.toUpperCase();
            }
            else {
                String newChar = "";
                newChar += c;
                name += newChar.toLowerCase();
            }
        }
        return name;
    }

    public static Employee addRandomEmployee() {
        String firstName = createRandomName();
        String lastName = createRandomName();

        int age = (int) ((Math.random() * 40) + 18);

        return new Employee(firstName, lastName, age);

    }

    public static void main(String[] args) {

        List<Employee> employees = new ArrayList<Employee>();

        for (int i = 0; i < 20; i++) {
            employees.add(addRandomEmployee());
        }

        employees.forEach(employee -> System.out.println(employee.getDetails()));

        System.out.println("===========");

        Stream<String> employeeNames = employees.stream()
                .map(employee -> employee.getFirstName() + " " + employee.getLastName());

        employeeNames.forEach(name -> System.out.println(name));

        System.out.println("===========");

        Stream<Employee> promotionsApplied = employees.stream()
                .map(employee -> employee.getAge() > 50 ? new Manager(employee.getFirstName(), employee.getLastName(), employee.getAge()) : employee);

        //        promotionsApplied.forEach(person -> System.out.println(person.getName() + " is a " + person.getClass()));

        // Can I make a stream a new list and then replace the old list with the new? Yes!!!

        List<Employee> promotedList = promotionsApplied.collect(Collectors.toList());

        employees = promotedList;

        System.out.println("===========");

        employees.forEach(employee -> System.out.println(employee.getDetails() + " is a " + employee.getClass()));


    }
}
