package qa.emp;

import java.util.Comparator;

public class Employee {

    private String firstName;
    private String lastName;
    private int age;

    public static int retirementAge = 65;

    public Employee(String lastName, int age) {
        this.lastName = lastName;
        this.age = age;
    }

    public Employee(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getName() {
        return firstName + " " + lastName;
    }

    public int getAge() {
        return age;
    }

    public void incAge() {
        if (this.age <= retirementAge) {
            this.age++;
        }
    }

    public static void setRetirementAge(int retirementAge) {
        Employee.retirementAge = retirementAge;
    }

    public String getDetails() {
        return getName() + "\t" + age + ";";
    }

    public static class EmployeeNameComparator implements Comparator<Employee> {

        @Override
        public int compare(Employee o1, Employee o2) {
//            return o1.lastName.compareTo(o2.lastName);

            int firstComparison = Integer.compare(o1.age, o2.age);

            if(firstComparison != 0) return firstComparison;

            int secondComparison = o1.lastName.compareTo(o2.lastName);

            if(secondComparison != 0) return secondComparison;

            return o2.firstName.compareTo(o2.firstName);
        }
    }

    // 12) Code your own Exception
    public static class UnderAgeException extends Exception {
        private int age;

        public int getAge() {
            return age;
        }

        public UnderAgeException(int age, String message) {
            super(message);
            this.age = age;
        }
    }
}
