package qa.emp;

import java.io.*;

public class EmployeeFileTestFinally {

    public static final String filePath = "./src/employees.txt";

    // Made a static BufferedReader so all methods in this class can access
    static BufferedReader bfr = null;
    static BufferedWriter bfw = null;

    public static String getInput(String prompt) throws IOException {

        System.out.println(prompt);

        InputStreamReader inputStream = new InputStreamReader(System.in);

        bfr = new BufferedReader(inputStream);

        String input = bfr.readLine();
//        bfr.close();
        return input;
    }

    public static Employee inputEmployee() throws IOException, Employee.UnderAgeException {
        String firstName = getInput("Please enter your first name: ");
        String lastName = getInput("Please enter your last name: ");

        String strAge = getInput("Please enter your age: ");
        int age = Integer.parseInt(strAge);

        if(age < 18) throw new Employee.UnderAgeException(age, "Age is too young");
        // Method returns employee for writing - could extend to allow manager details?
        return new Employee(firstName, lastName, age);
    }

    // Added writeEmployee method
    public static void writeEmployee(Employee employee) throws IOException {
        FileWriter fileWriter = new FileWriter(filePath, true);
        bfw = new BufferedWriter(fileWriter);
        String dataToWrite = employee.getDetails();
        bfw.write(dataToWrite + "\n");
        bfw.close();
    }

    public static void showEmployees() throws IOException {
        FileInputStream fileInput = new FileInputStream(filePath);

        InputStreamReader inputStream = new InputStreamReader(fileInput);

        bfr = new BufferedReader(inputStream);

        String data = bfr.readLine();

        while(data != null) {
            System.out.println(data);
            data = bfr.readLine();
        }

        bfr.close();
    }


    public static void main(String[] args) {

        BufferedReader optionInput = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.println("Would you like to enter or show employees? e to enter, s to show, q to quit");
            String option = optionInput.readLine();

            while(option.toLowerCase().compareTo("q") != 0) {
                if(option.toLowerCase().compareTo("e") == 0) {
                    Employee newEmployee = inputEmployee();
                    writeEmployee(newEmployee);
                }
                if(option.toLowerCase().compareTo("s") == 0) {
                    showEmployees();
                }
                System.out.println("Would you like to enter or show employees? e to enter, s to show, q to quit");
                option = optionInput.readLine();
            }
        }
        catch(Employee.UnderAgeException e) {
            System.out.println(e.getMessage());
        }
        catch(FileNotFoundException e) {
            System.out.println("The file is not found");
        }
        catch(IOException error) {
            System.out.println("Problem with file handling");
        }
        catch(NumberFormatException e) {
            System.out.println(e.getMessage() + " is an invalid age, data has not been written");
        }
        finally {
            try {
                optionInput.close();
                bfr.close();
                bfw.close();
            }
            catch(IOException e) {
                System.out.println("There was an error");
            }
        }
    }
}
