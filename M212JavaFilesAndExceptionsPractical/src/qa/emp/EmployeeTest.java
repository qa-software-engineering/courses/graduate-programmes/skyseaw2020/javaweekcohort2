package qa.emp;

import java.util.*;

public class EmployeeTest {
    public static Employee addRandomEmployee() {
        String firstName = "";
        String lastName = "";

        int age = (int) ((Math.random() * 40) + 18);

        int randomFirstNameLength = (int) ((Math.random() * 10));
        randomFirstNameLength = randomFirstNameLength == 0 ? (int) 0.6 * 10 : randomFirstNameLength;
        for(int j = 0; j < randomFirstNameLength; j++) {
            char c = (char) ((Math.random() * 26) + 65);
            if ( j == 0 ) {
                firstName += c;
                firstName = firstName.toUpperCase();
            }
            else {
                String newChar = "";
                newChar += c;
                firstName += newChar.toLowerCase();
            }

        }

        int randomLastNameLength = (int) ((Math.random() * 10));
        randomLastNameLength = randomLastNameLength == 0 ? (int) 0.6 * 10 : randomLastNameLength;

        for(int k = 0; k < randomLastNameLength; k++) {
            char c = (char) ((Math.random() * 26) + 65);
            if (k == 0) {
                lastName += c;
                lastName = lastName.toUpperCase();
            }
            else {
                String newChar = "";
                newChar += c;
                lastName += newChar.toLowerCase();
            }
        }

        double managerOrEmployee = Math.random();

        if(managerOrEmployee < 0.5) {
            return new Employee(firstName, lastName, age);
        } else {
            return new Manager(firstName, lastName, age);
        }
    }

    public static void main(String[] args) {

//        Employee employee1 = new Employee("John","Smith", 21);
//        Employee employee2 = new Employee("Jane", "Jones", 42);
//        Manager manager1 = new Manager("Michelle", "Management", 33);
//
//        manager1.addEmployee(employee1);
//        manager1.addEmployee(employee2);
//
//        Set<Employee> employees = new HashSet<Employee>();
//
//        employees.add(employee1);
//        employees.add(employee2);
//        employees.add(manager1);
//
//        for(Employee employee : employees) {
//            System.out.println(employee.getDetails());
//        }
//
//        long start = System.currentTimeMillis();
//
//        for(int i = 0; i < 999999; i++) {
//            employees.add(addRandomEmployee());
//        }
//
//        long end = System.currentTimeMillis();
//
//        System.out.println("It took " + ( end - start ) + " milliseconds to compile the list");
//
//        List<Employee> employeesList = new ArrayList<Employee>();
//
//        start = System.currentTimeMillis();
//
//        for(int i = 0; i < 999999; i++) {
//            employees.add(addRandomEmployee());
//        }
//
//        end = System.currentTimeMillis();
//
//        System.out.println("It took " + ( end - start ) + " milliseconds to compile the list");
//
//        int key = 0;
//
//        Map<Integer, Employee> employeesMap = new HashMap<Integer, Employee>();
//
//        start = System.currentTimeMillis();
//
//        for(int i = 0; i < 999999; i++) {
//            employeesMap.put(i, addRandomEmployee());
//        }
//
//        end = System.currentTimeMillis();
//
//        System.out.println("It took " + ( end - start ) + " milliseconds to compile the list");

        List<Employee> employeesToCompare = new ArrayList<Employee>();

        for(int i = 0; i < 20; i++) {
            employeesToCompare.add(addRandomEmployee());
        }

        for(Employee employee: employeesToCompare) {
            System.out.println(employee.getDetails());
        }

        Collections.sort(employeesToCompare, new Employee.EmployeeNameComparator());

//        Collections.sort(employeesToCompare, new Comparator<Employee>() {
//            @Override
//            public int compare(Employee o1, Employee o2) {
//                return Integer.compare(o1.getAge(), o2.getAge());
//            }
//        });
//
//        Collections.sort(employeesToCompare, new Comparator<Employee>() {
//            @Override
//            public int compare(Employee o1, Employee o2) {
//                return o1.getLastName().compareTo(o2.getLastName());
//            }
//        });

        System.out.println("======");

        for(Employee employee: employeesToCompare) {
            System.out.println(employee.getDetails());
        }

    }
}
