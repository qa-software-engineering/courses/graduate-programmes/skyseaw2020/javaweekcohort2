package qa.emp;

import java.io.*;

public class EmployeeFileTest {

    // 2 i) Define a static method called getInput that takes a String for the prompt to display - will return a string
    public static String getInput(String prompt) throws IOException { // 2 v) Compile, fix and pass the buck!
        // 2 ii) Print prompt to console
        System.out.println(prompt);

        // 2 iii) Declare and create InputStreamReader using System.in as constructor argument
        InputStreamReader inputStream = new InputStreamReader(System.in);

        // 2 iv) Declare and create BufferedReader using inputStream as reference
        BufferedReader reader = new BufferedReader(inputStream);

        // 2 iv) Use readLine() method of BufferedReader to create string and return it
        String input = reader.readLine();
        return input;
    }

    // 3) Add empty inputEmployee and showEmployees methods

    public static void inputEmployee() throws IOException, Employee.UnderAgeException { // 5) Pass the buck
        // 4 i)
        String firstName = getInput("Please enter your first name: ");
        String lastName = getInput("Please enter your last name: ");

        // 4 ii)
        String strAge = getInput("Please enter your age: ");
        int age = Integer.parseInt(strAge);

        if(age < 18) throw new Employee.UnderAgeException(age, "Age is too young");
    }

    // 9 ii) Create and define filePath String
    public static final String filePath = "./src/employees.txt";

    public static void showEmployees() throws IOException { // 10 iv) Make method throw exception
        // 9 iii) Create FileInputStream with filePath as argument
        FileInputStream fileInput = new FileInputStream(filePath);

        // 9 iv) Create InputStreamReader with fileInput as argument
        InputStreamReader inputStream = new InputStreamReader(fileInput);

        // 9 v) Create BufferedReader with inputStream as argument
        BufferedReader reader = new BufferedReader(inputStream);

        // 10 i) Declare data and set to reader.readLine()
        String data = reader.readLine();

        // 10 ii) While data not null, print out the line and read again
        while(data != null) {
            System.out.println(data);
            data = reader.readLine();
        }

        // 10 iii) Close the reader
        reader.close();
    }


    public static void main(String[] args) {

        // 6 i) TryCatch
        try {
            inputEmployee();
            // 11) Call showEmployees
            showEmployees();
        }
        // 14) Handle the UnderAgeException
        catch(Employee.UnderAgeException e) {
            System.out.println(e.getMessage());
        }
        // 11 ii) Intercept IOException with FileNotFoundException
        catch(FileNotFoundException e) {
            System.out.println("The file is not found");
        }
        catch(IOException error) {
            // 6 iii)
            System.out.println("Problem with file handling");
        }
        // 7 i) Handle NumberFormatException
        catch(NumberFormatException e) {
            System.out.println(e.getMessage() + " is an invalid age");
        }
    }
}
