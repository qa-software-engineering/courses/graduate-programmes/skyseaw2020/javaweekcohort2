package sorting;

import java.util.ArrayList;
import java.util.Collections;

public class Sorting {

    public static void main(String[] args) {
        ArrayList<Integer> intList = new ArrayList<>();

        intList.add(4);
        intList.add(193);
        intList.add(23);
        intList.add(234);

        System.out.println(intList);

        Collections.sort(intList);

        System.out.println(intList);

        for(int i = 0; i < intList.size(); i++) {
            System.out.println(intList.get(i) + ", ");
        }
    }
}
