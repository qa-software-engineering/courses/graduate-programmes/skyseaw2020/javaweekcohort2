package sets;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class Sets {
    public static void main(String[] args) {

        int count[] = {34, 22, 10, 60, 30, 22};

        Set<Integer> set = new HashSet<>();

        try {
            for(int i = 0; i < count.length; i++) {
                set.add(count[i]);
            }

            System.out.println(set);

            TreeSet<Integer> sortedSet = new TreeSet<>(set);
            System.out.println(sortedSet);

            System.out.println((Integer)sortedSet.first());
            System.out.println((Integer)sortedSet.last());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // LinkedHashSet
        LinkedHashSet<String> hs = new LinkedHashSet<>();

        hs.add("E");
        hs.add("A");
        hs.add("B");
        hs.add("C");
        hs.add("D");

        System.out.println(hs);
    }
}
