package lists;

import java.util.ArrayList;

public class BookMain {

    public static void main(String[] args) {

        Book b = new Book("My Story", "Lewis Hamilton", 1999);

        ArrayList<Book> bookList = new ArrayList<>();

        bookList.add(new Book("Hungry Catapillar", "Eric Karle", 499));
        bookList.add(b);

        for(Book book: bookList) {
            System.out.println(book);
            System.out.println(book.getTitle());
        }

        Book[] bookArray = {b};
        System.out.println(bookArray[0]);

        System.out.println(bookList.get(0));

        for(Book book: bookList) {
            if(book.getTitle().equals("My Story")) {
                System.out.println("Index of book " + book.getTitle() + ": " + bookList.indexOf(book));
            }
        }

        bookList.remove(0);
        bookList.remove(b);

    }
}
