package generics;

public class GenericMethods {

//    public static void printArray(Integer[] inputArray) {
//        // implementation
//        for(Integer element: inputArray) {
//            System.out.printf("%s ", element);
//        }
//    }
//
//    public static void printArray(Double[] inputArray) {
//        // implementation
//        for(Double element: inputArray) {
//            System.out.printf("%s ", element);
//        }
//    }
//
//    public static void printArray(Character[] inputArray) {
//        // implementation
//        for(Character element: inputArray) {
//            System.out.printf("%s ", element);
//        }
//    }

    public static <E> void printArray(E[] inputArray) {
        for(E element : inputArray) {
            System.out.printf("%s ", element);;
        }
    }

    public static void main(String[] args) {

        Integer[] intArray = {1, 2, 3, 4 ,5};

        Double[] doubleArray = {1.1, 1.2, 1.3, 1.5, 1.5};

        Character[] charArray = {'H', 'E', 'L', 'L', 'O'};

        printArray(intArray);
        printArray(doubleArray);
        printArray(charArray);

        String[] stringArray = {"Hello", "World", "!"};

        printArray(stringArray);

//        Book[]

    }
}
