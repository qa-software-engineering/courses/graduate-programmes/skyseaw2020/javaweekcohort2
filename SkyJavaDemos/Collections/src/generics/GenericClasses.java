package generics;

public class GenericClasses <T extends Animal>{

    T genericObject;

    public GenericClasses(T genericObject) {
        this.genericObject = genericObject;
    }

    public T getGenericObject() {
        return genericObject;
    }

    public static void main(String[] args) {

        GenericClasses<Rabbit> an1 = new GenericClasses<>(new Rabbit("Peter", 2, true));
        GenericClasses<Animal> an2 = new GenericClasses<>(new Rabbit("Benjy", 3, false));
        GenericClasses<Book> an3 = new GenericClasses<>(new Book("GOT1", "A Game of Thrones", "George RR Martin", 1199));

        System.out.println(an1.getGenericObject());
        System.out.println(an2.getGenericObject());
        System.out.println(an3.getGenericObject());
    }
}
