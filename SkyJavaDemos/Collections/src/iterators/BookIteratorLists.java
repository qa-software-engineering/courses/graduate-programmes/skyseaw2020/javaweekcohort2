package iterators;

import lists.Book;

import java.util.ArrayList;
import java.util.Iterator;

public class BookIteratorLists {

    public static void main(String[] args) {

        ArrayList<Book> bookList = new ArrayList<Book>();
        bookList.add(new Book("A Game of Thrones", "George RR Martin", 799));
        bookList.add(new Book("A Clash of Kings", "George RR Martin", 899));
        bookList.add(new Book("A Storm of Swords", "George RR Martin", 899));
        bookList.add(new Book("A Feast for Crows", "George RR Martin", 899));

        Iterator<Book> iter = bookList.iterator();

        while(iter.hasNext()) {
            System.out.println(iter.next());
        }




    }
}
