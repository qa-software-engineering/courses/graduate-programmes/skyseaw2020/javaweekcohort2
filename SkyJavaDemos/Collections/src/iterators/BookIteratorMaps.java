package iterators;

import lists.Book;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class BookIteratorMaps {

    public static void main(String[] args) {

        Map<String, Book> bookMap = new HashMap<>();

        bookMap.put("Book 1", new Book("A Game of Thrones", "George RR Martin", 799));
        bookMap.put("Book 2", new Book("A Clash of Kings", "George RR Martin", 899));
        bookMap.put("Book 3", new Book("A Storm of Swords", "George RR Martin", 899));
        bookMap.put("Book 4", new Book("A Feast for Crows", "George RR Martin", 899));

        Iterator<Book> iter = bookMap.values().iterator();

        while(iter.hasNext()) {
            System.out.println(iter.next());
        }
    }
}
