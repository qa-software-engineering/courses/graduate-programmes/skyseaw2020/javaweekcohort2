package comparable;

import java.util.ArrayList;
import java.util.Collections;

public class BookMainList {

    public static void main(String[] args) {

        Book book1 = new Book ("A Game of Thrones", "George RR Martin", 799);
        Book book2 = new Book ("A Clash of Kings", "George RR Martin", 899);
        Book book3 = new Book ("A Storm of Swords", "George RR Martin", 999);
        Book book4 = new Book ("A Feast for Crows", "George RR Martin", 899);
        Book book5 = new Book("A Dance with Dragons", "George RR Martin", 1199);

        ArrayList<Book> bookList = new ArrayList<>();
        bookList.add(book1);
        bookList.add(book2);
        bookList.add(book3);
        bookList.add(book4);
        bookList.add(book5);

        Collections.sort(bookList, new CompareBooksByPrice());

        for(Book book: bookList) {
            System.out.println(book.getTitle() + ": " + book.getPrice());
        }


    }
}
