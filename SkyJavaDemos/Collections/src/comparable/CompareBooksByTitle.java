package comparable;

import java.util.Comparator;

public class CompareBooksByTitle implements Comparator<Book> {


    @Override
    public int compare(Book book1, Book book2) {
        if (book1.getTitle() < book2.getTitle()){
            return -1;
        } else if (book1.getTitle() > book2.getTitle()){
            return 1;
        }

        return 0;
    }
}
