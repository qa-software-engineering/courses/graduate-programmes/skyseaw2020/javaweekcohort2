public class LambdaExample1 {

    public static void main(String[] args) {

        LambdaExample1 tester = new LambdaExample1();

        // With type declaration
        MathOperation add = (int a, int b) -> a + b;
        System.out.println("10 + 5 = " + tester.operate(10, 5, add));

        // Without type declaration
        MathOperation subtract = (a, b) -> a - b;
        System.out.println("10 - 5 = " + tester.operate(10, 5, subtract));

        // with return statement and {}
        MathOperation multiply = (int a, int b) -> {
            System.out.println("Doing a multiply");
            return a * b;
        };
        System.out.println("10 * 5 = " + tester.operate(10, 5, multiply));

        // with {}
        MathOperation divide = (int a, int b) -> { return a / b; };
        System.out.println("10 / 5 = " + tester.operate(10, 5, divide));

        System.out.println("10 % 5 = " + tester.operate(10, 5, (a, b) -> a % b));
    }

    interface MathOperation {
        int operation(int a , int b);
    }

    private int operate(int a, int b, MathOperation mathOperation) {
        return mathOperation.operation(a, b);
    }
}
