import java.util.ArrayList;

public class LambdaExample2 {

    public static void main(String[] args) {

        ArrayList<Integer> tempList = new ArrayList<Integer>();
        ArrayList<Integer> intList = new ArrayList<Integer>();
        intList.add(1);
        intList.add(2);
        intList.add(3);
        intList.add(4);
        intList.add(5);

        // Previously, to loop through an array list:
		for(int i = 0; i < intList.size(); i++) {
			tempList.add(intList.get(i) + 1);
		}

		intList = tempList;

		// Using lambdas
        intList.forEach(i -> tempList.add(i + 1));

        intList.replaceAll(i -> i * 10);

    }
}
