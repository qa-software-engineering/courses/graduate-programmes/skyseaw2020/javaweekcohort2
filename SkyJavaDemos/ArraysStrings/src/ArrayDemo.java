public class ArrayDemo {

    public static void main(String[] args) {

        int[] arrOfInts = new int[7]; // specify length of the array in []
        int[] arrOfInts2 = {10, 20, 30, 40, 50, 60, 70};

        String[] arrOfStrings = new String[8];
        String[] arrOfStrings2 = {"Rod", "Jane", "Freddie"};

        int a = arrOfInts2[3];
        System.out.println(a);

        arrOfInts2[3] = 45;

//        System.out.println(arrOfStrings2[3]);

//        for(int i = 0; i < arrOfStrings2.length; i++) {
//            System.out.println(arrOfStrings2[i]);
//        }

        for(String name : arrOfStrings2) {  // foreach (each element in array after the colon) aka for of/ for in
            System.out.println(name);
        }


    }

    public static int printArrayElement(int[] numbers, int element) {
        int number = 0;
        if(element < numbers.length) {
            number = numbers[element];
        }
        return number;
    }
}
