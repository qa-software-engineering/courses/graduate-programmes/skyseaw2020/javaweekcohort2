package application;

public class ArrayUtils {

    public static int findHighest(int[] array) throws RuntimeException {

        if(array.length == 0) throw new RuntimeException("Idiot! The array passed was empty!");

         int highestFound = Integer.MIN_VALUE;

         for(int i = 0; i < array.length; i++) {
             if(array[i] > highestFound) {
                 highestFound = array[i];
             }
         }

         return highestFound;
    }
}
