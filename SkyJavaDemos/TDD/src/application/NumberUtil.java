package application;

public class NumberUtil {

    public static void main(String[] args) {

        String[] testCases = {"0", "+10", "-10", "1.2", ".12", "1,2", "1 2", "abc", "12abc", "abc12", "1+2", "1-2", "1*2", "1/2", "" };

        for(String testCase : testCases) {
            System.out.println("Test case:\t" + testCase + "; \t\t\t\t isNumeric result: " +isNumeric(testCase));
        }
    }

    public static boolean isNumeric(String str) {
        return str.matches("[+-]?\\d*(\\.\\d+)?");
    }

}
