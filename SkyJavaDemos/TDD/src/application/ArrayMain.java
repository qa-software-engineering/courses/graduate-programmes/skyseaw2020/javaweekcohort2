package application;

public class ArrayMain {

    public static void main(String[] args) {
        int[] array = {};

        try {
            System.out.println(ArrayUtils.findHighest(array));
        }
        catch(IllegalArgumentException e) {

        }
        catch(RuntimeException e) {
            System.out.println(e.getMessage());
        }
        catch(Throwable e) {

        }
        finally {
            System.out.println("The programme has completed.");
        }
    }
}
