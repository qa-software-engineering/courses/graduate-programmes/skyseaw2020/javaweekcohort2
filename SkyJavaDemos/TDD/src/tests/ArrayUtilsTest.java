package tests;

import application.ArrayUtils;
import org.junit.Before;
import org.junit.Test;
//import org.junit.matchers.JUnitMatchers.

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class ArrayUtilsTest {

    // AAA - test method

    // Arrange - set up the for the test (fixtures, mocks, etc)

    // Act - execute CUT - Class Under Test or Code Under Test and record result

    // Assert - Check the expected outcome matches the actual outcome
    public int[] array;

//    @Before
//    public void setup() {
//        array = {10};
//    }


    @Test
    public void findHighestInArrayOfOne() {
        // Arrange
        int[] array = {10};

        // Act
        int result = ArrayUtils.findHighest(array);

        // Assert
        assertThat(result, is(10));
    }

    @Test
    public void findHighestInArrayOfTwo() {
        // Arrange
        int[] array = {10, 20};

        // Act
        int result = ArrayUtils.findHighest(array);

        // Assert
        assertThat(result, is(20));
    }
}
