package tests;

import application.ArrayUtils;
import org.junit.Test;

public class ArrayUtilsExceptionTest {

    @Test(expected = RuntimeException.class) // Assert
    public void findHighestInEmptyArrayThrows() {

        // Arrange
        int[] array = {};

        // Act
        ArrayUtils.findHighest(array);

    }
}
