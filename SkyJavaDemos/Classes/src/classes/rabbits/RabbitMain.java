package classes.rabbits;

public class RabbitMain {

    public static void main(String[] args) {
        Rabbit thumper = new Rabbit();

        Rabbit flopsy = new Rabbit( "Flopsy", 2,"Grey");

        Rabbit peter = new Rabbit("Peter", 4, "White");

        System.out.println(thumper.getAge());
        // Bad ENCAPSULATION!
        thumper.setAge(1);
        thumper.name = "Thumper";

        System.out.println(thumper.toString());    // implicit call to the method toString()
        System.out.println(flopsy);
        System.out.println(peter);

        thumper.setAge(30);
        System.out.println(thumper);

        thumper.name = "Peter";
        thumper.setAge(2);
        System.out.println(thumper);

    }
}
