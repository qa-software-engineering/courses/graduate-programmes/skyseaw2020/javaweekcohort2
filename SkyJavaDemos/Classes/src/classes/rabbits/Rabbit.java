package classes.rabbits;

public class Rabbit {

    // Properties/members
    public String name = "Default";        // null
    private int age;            // 0
    private String colour;      // null
    private boolean isFuzzy;

    // Constructor
    public Rabbit() { }

    // Overloading - polymorphism
    public Rabbit(String name, int age, String colour) {
        this.name = name;
        this.age = age;
        this.colour = colour;
        this.isFuzzy = true;
    }

    public Rabbit(String name, int age, String colour, boolean isFuzzy) {
        this.name = name;
        this.age = age;
        this.colour = colour;
        this.isFuzzy = isFuzzy;
    }

    // Getter
    public int getAge() {
        return this.age;
    }

    // Setter
    public void setAge(int newAge) {
        if(newAge < 25) {
            this.age = newAge;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public boolean isFuzzy() {
        return isFuzzy;
    }

    public void setFuzzy(boolean fuzzy) {
        isFuzzy = fuzzy;
    }

//    @Override
//    public String toString() {
//        String returnValue = "Rabbit details: \tName: " + name + "\t Age: " + age;
//        return returnValue;
//    }


    @Override
    public String toString() {
        return super.toString() + " : " +
                "Rabbit{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", colour='" + colour + '\'' +
                ", isFuzzy=" + isFuzzy +
                '}';
    }
}
