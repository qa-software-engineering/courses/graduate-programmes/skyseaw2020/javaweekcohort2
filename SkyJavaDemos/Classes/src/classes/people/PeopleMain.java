package classes.people;

public class PeopleMain {

    public static void main(String[] args) {

        Person billy = new Person("Billy", 21);

        int birthdays = 32;

        peoplePrinter(birthdays);       // => 32 on console then 33

        System.out.println(birthdays);  // => 32

        peoplePrinter(65);

        System.out.println(birthdays);  // 32

        birthdays = 24;

        birthdays = PeopleMain.peoplePrinter(birthdays);

        System.out.println(birthdays);  // 25
    }

    public static int peoplePrinter(int birthdays /* = whatever number is passed in */) {
        // birthdays is a variable LOCAL to this method
        System.out.println(birthdays++); // 32
        System.out.println(birthdays);  // 33

        return birthdays;
    }
}
