package anonymous;

public class MyClass {
    // method which accepts the object of interface Message
    public void displayMessage(Message m) {
        System.out.println(m.greet() +
                ", This is an example of anonymous inner class as an argument");
    }

    public static void main(String args[]) {
        // Instantiating the class
        MyClass obj = new MyClass();

        String defaultMessage = "Default message";
        String someMessage = "Some Message";
        String space = " ";

        StringBuilder sb = new StringBuilder();

        sb.append("My");
        sb.append(space);
        sb.append("head");
        sb.append(space);
        sb.append("hurts!");

        // Passing an anonymous inner class as an argument
        obj.displayMessage(() -> defaultMessage);

        obj.displayMessage(new Message() {
            public String greet() { return sb.toString();}

        });
    }

}
