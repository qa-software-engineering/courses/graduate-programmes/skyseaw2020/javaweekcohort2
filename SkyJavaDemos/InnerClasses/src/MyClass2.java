public class MyClass2 {

    public static void main(String args[]) {
        // Instantiating the outer class
        OuterDemo2 outer = new OuterDemo2();

        // Instantiating the inner class
        OuterDemo2.InnerDemo inner = outer.new InnerDemo();
        System.out.println(inner.getNum());
    }
}
