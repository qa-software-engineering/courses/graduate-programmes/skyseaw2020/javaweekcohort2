public class OuterDemo3 {

    // instance method of the outer class
    public void my_Method() {
        int num = 23;

        // method-local inner class
        class MethodInnerDemo {
            public void print() {
                System.out.println("This is method inner class "+num);
            }
        } // end of inner class

        // Accessing the inner class
        MethodInnerDemo inner = new MethodInnerDemo();
        inner.print();

    }

    public static void main(String args[]) {
        OuterDemo3 outer = new OuterDemo3();
        outer.my_Method();
    }
}
