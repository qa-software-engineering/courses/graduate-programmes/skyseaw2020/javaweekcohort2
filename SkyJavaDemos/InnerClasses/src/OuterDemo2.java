public class OuterDemo2 {

    // private variable of the outer class
    private int num = 175;

    // inner class
    public class InnerDemo {
        public int getNum() {
            System.out.println("This is the getnum method of the inner class");
            return num;
        }
    }
}
