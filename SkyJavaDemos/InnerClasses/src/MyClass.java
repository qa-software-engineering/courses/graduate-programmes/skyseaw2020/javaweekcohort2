public class MyClass {

    public static void main(String args[]) {
        // Instantiating the outer class
        OuterDemo outer = new OuterDemo();

//         OuterDemo.InnerDemo inner = outer.new InnerDemo();
        // Not allowed as InnerDemo is private

//        System.out.println(outer.num);

        // Accessing the display_Inner() method.
        outer.displayInner();

//        OuterDemo.InnerDemo innerInstance = outer.getInnerDemo();
    }
}