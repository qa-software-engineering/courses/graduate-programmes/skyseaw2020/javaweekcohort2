public class OuterDemo {

    private int number = 22;

    // inner class
    private class InnerDemo {
        public void print() {
            System.out.println("This is an inner class");
        }
    }

    // Accessing he inner class from the method within
    public void displayInner() {
        InnerDemo inner = new InnerDemo();
        inner.print();
    }

    public InnerDemo getInnerDemo() {
        return new InnerDemo();
    }
}

