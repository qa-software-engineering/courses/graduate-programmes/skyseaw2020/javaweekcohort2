package com.basics;

import java.util.Date;

public class Basics {

    // Comments

    // Adding a single line comment

    /* Adding a comment
     * that can span over lines
     * Added at the start automatically
     */

    /**
     * JavaDoc comment
     * Used by a utility to produce documentation for your code
     * @Method methodName()
     */

    public static void main(String[] args) {

        // Variables

        int age = 5;             // Type and name with assignment
        double vat = 17.5;       // Type and name with assignment

        char anyThing;           // Type and name - no assignment

        boolean over40, male;    // Chained with no assignment

        // Variable Names - use camel-casing; case-sensitive;

        boolean anyCombinationOfLetters;
        boolean anyCombinationOfLettersOrNumbers1234;

        boolean numbersAtTheStart;                      // eg int 1number; int 23numbers;
        boolean includingSpecialCharacters;             // eg int #myVar; int my*Var; int my-var;

        boolean reservedKeywords;                       // eg class, public, boolean, int;

        // Characters - any 16-bit unicode character surrounded with single quotes

        char aChar = 'a';
        char numericChar = '2';
        char specialChar = '"';

        // Boolean
        boolean trueValue = true;
        boolean falseValue = false;

        // Number Types - there are 6 numeric types

        // Integers
        byte eightBitSignedInteger;
        short sixteenBitSignInteger;
        int thirtyTwoBitSignedInteger;
        long sixtyFourBitSignedInteger;

        // Decimals
        float thirtyTwoBitSinglePrecisionFloatingPointNumber;
        double sixtyFourBitSinglePrecisionFloatingPointNumber;

        // Other types - arrays and classes
        String myString = "10";    // Included by default
        Date myDate;        // Have to import

        int[] arrayOfIntegers;
        String[] arrayOfStrings;
        Date[] arrayOfDates;

    }
}
