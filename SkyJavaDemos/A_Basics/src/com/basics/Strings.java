package com.basics;

public class Strings {

    public static void main(String[] args) {

        // Strings
        String hello = "Hello ";
        String world = "World";

        String greeting = hello + world;
        System.out.println(greeting);

        // Strings and other variables
        String start = "My age is ";
        int age = 21;

        String complete = start + age;
        System.out.println(complete);

        // Comparing Strings
        System.out.println(hello.equals(world));

        String world2 = "world";
        System.out.println(world.equalsIgnoreCase(world2));

    }
}
