package com.basics;

import java.math.BigDecimal;

public class Operators {


    public static void main(String[] args) {

//        // Declare and change variable assignment
//
//        // Declare variable
//        int age = 16;
////                System.out.println(age);
//
//        // Change variable value
//        age = 5;
////        System.out.println(age);
//
//        // Simple Operators
//        int a = 1 + 5; // Addition
//        int b = 1 - 5; // Subtraction
//        int c = 1 * 5; // Multiplication
//        int d = 1 / 5; // Division
//        int e = 1 % 5; // Modulo
//
////        System.out.println(10 % 7);
//
//        // Compound Operators
//        a += 5; // equivalent to a = a + 5;
//        b -= 5; // equivalent to b = b - 5;
//        c *= 5; // equivalent to c = c * 5;
//        d /= 5; // equivalent to d = d / 5;
//
//        // Pre and post increment/decrement
////        System.out.println(a);
////        System.out.println(a++);    // 11
////        System.out.println(a);      // 12
////        System.out.println(++a);    // 13
//        a++; // returns a and adds 1 to it
//        b--; // returns b and subtracts 1 from it
//
//        int var1 = 3, var2 = 0;
//
//        var2 = ++var1;    // Adds 1 to var1 and then sets var2 = var1
////        System.out.println("Var1: " + var1 + " Var2: " + var2);
//
//        var2 = var1++;    // Sets var2 to var1 and then adds 1 to var1
////        System.out.println("Var1: " + var1 + " Var2: " + var2);
//
//        // Comparison Operators
//        int x = 10, y = 100;
//
////        System.out.println(x > y);        // Greater than
////        System.out.println(x < y);        // Less than
////        System.out.println(x >= y);        // Greater than or equal to
////        System.out.println(x <= y);        // Less than or equal to
////        System.out.println(x != y);        // Not equal to
////        System.out.println(x == y);        // Equal to
////        System.out.println(x = y);
////        System.out.println(x);
//
//        // Logical Operators
//        age = 20;
//        int val = 7;
//
////        System.out.println((age >= 18) && (val == 7));
////        System.out.println((age >= 18) || (val == 7));
////        System.out.println((age <= 18) || (val == 7));
////        System.out.println((age <= 18) && (val == 7));
////        System.out.println((age >= 18) && !(val == 8));
//
//        // Operator Order Precedence BODMAS/BIDMAS
//        a = 1 + 5 * 2;
//        b = (1 + 5) * 2;
//        System.out.println(a);
//        System.out.println(b);

        String num1 = "0.1";
        String num2 = "0.2";

        BigDecimal num1BD = new BigDecimal(num1);
        BigDecimal num2BD = new BigDecimal(num2);

        System.out.println(num1 + num2);
        System.out.println(num1BD.add(num2BD));

    }
}
