import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsExample {

    public static void main(String[] args) {

        List<String> myList = Arrays.asList("a1", "a2", "b1", "c2", "c1");
        List<Integer> intList = Arrays.asList(1, 2, 3, 4, 5);

        // Collections have a stream method
        // Converts collection into a stream of values

        myList
                .stream()                       // Start the the stream
                .map(String::toUpperCase)       // Map the stream - take each element received and convert to upper case
                .filter(s -> s.startsWith("C")) // Only allow strings that start with a capital C through to the next stage
                .sorted()                       // Sort the values in the list
                .forEach(System.out::println);  // Output the final list as each value is received

        System.out.println(myList.toString());

        List<Integer> mappedFilterList = intList
                                                .stream()
                                                .map(i -> i * i)
                                                .filter(i -> i % 2 == 0)
                                                .collect(Collectors.toList());

        System.out.println(mappedFilterList);

        String total = intList
                            .stream()
                            .map(i -> i * i)
                            .filter(i -> i % 2 == 0)
                            .collect(Collectors.summarizingInt(i -> i))
                                    .toString();

        System.out.println("Statistics: " + total);

        Integer reducedList = intList.stream().reduce(1, (x, a) -> a * x);
        System.out.println("Output: " + reducedList);
    }
}
