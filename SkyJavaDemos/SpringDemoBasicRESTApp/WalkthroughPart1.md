# Creating a Simple Spring Boot REST Application - Part 1 - Setting Up and Running

## Project Set Up

1. Go to [Spring Initializer](https://start.spring.io)
2. Change project metadata so it uses Java 8
3. Add Spring Web to dependencies
4. Click Generate
5. Copy contents of downloaded `demo` folder into a suitable **project** folder
6. Open Project in IntelliJ
7. Explain about `pom.xml`.

## Run Basic Application

1. Open class `DemoApplication`
2. Explain that the `@SpringBootApplication` notation is used:
   - use auto-configuration
   - scanning for components
   - defining extra configuration in an application class
   - [See here](https://docs.spring.io/spring-boot/docs/current/reference/html/using-spring-boot.html#using-boot-using-springbootapplication-annotation)
3. Run DemoApplication and open browser to [http://localhost:8080] - expect *Whitelabel Error Page* - ask why?
   - Discuss **routes** and how these could be created

## Create a Controller Class to Specify Routes

1. Create a package called `controllers`
2. Add a **Class** called `DemoController`
3. Annotate the `DemoController` with `@RestController`
   - `@RestController` tells Spring that this code describes an endpoint that should be made available over the web.
4. Add a method called `hello` as shown:

```java
    /**
    * Makes GET requests to the /hello route return a String
    * The @GetMapping annotation tells Spring to use the method defined beneath it for GET requests to /hello
    * The @ResponseBody annotation on the return type binds the return value to the web response body.
    * The @RequestParam tells Spring how to deal with a ?name=<someValue> extension to the /hello route and 
    * tells the method to use a defaultValue of "World" if none is supplied.
    * This annotates the String argument name in the hello method.
    *
    * @param    name    a String that is either the value from the GET request or "World"
    * @return           a String that contains the name parameter
    */

    @GetMapping("/")
    public @ResponseBody String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        return String.format("Hello %s!", name);
    }
```

## Activity

Create a response on another route e.g. `/test` that takes *at least 2* `Request Parameters` and return's their value in a `String`.

**Activity Timebox:** *5 minutes*

Take a break!
