# Creating a Simple Spring Boot REST Application - Part 3 - Making and Testing a Service

## Services Introduction

Explain that dealing with requests in the Controller is not very scalable.  Different parts of the application will require different types of data.  Set up a Service class for each of the different requirements.

Let's do this in a TDD manner!

### The GreetingService

#### GreetingService Test
The service tests are nothing more than Java Unit tests.

1. Create a class called `GreetingServiceTest` in `test/java/com.example.demo.services` 
2. Write test to:
   - Instantiate instance of `GreetingService`
   - Define `result` and call `greet()`
   - `assertThat` `result` `is` "Well, hello there"

#### GreetingService

1. Define class `GreetingService` in `main/java/com.example.demo.services` annotating it with `@Service`;
2. Stub a method greet() that returns an empty String;
3. Run the GreetingServiceTest and then fix.

### The GreetingController

1. Create a class called `GreetingControllerTest` in `test/java/com.example.demo.controllers`;
2. Annotate the class with `@WebMvcTest` and metadata `(GreetingController.class)` - this annotation disables full auto-configuration and only applys the configuration neededfor these tests - in this case the `GreetingController`;
3. Add annotation `@Autowired` to a private MockMvc mockMvc - this is provided by the `@WebMvcTest` annotation;
4. Annotate an `private` instance of the `GreetingService` class as `greetingService` with `@MockBean` - this allows Spring to mock the class and record and vrerify behaviours on it - this is imported from mockito.MockBean in the spring framework;

We want the GreetingService to return some data based when one of it's methods is called - eg. a method called `greet()` which will simply return a `String` in our case.  Later we will see how this can make calls to a database, etc.

5. Define a `@Test` called `greetingShouldReturnMessageFromService()`;
6. The test arranges the mock and says `when(greetingService.greet()).thenReturn("Hello Mock");` - essentially when our test of the `GreetingController` class makes a call to `GreetingService`'s `greet` method it will be intercepted and the return replaced as we have specified - this increases the unit nature of the test;
7. Call `perform` on the `mockMvc` passing in `get("/greeting")` which will be the mapped route in the Controller - fix error by adding `throws Exception
8. Chain a call to `andDo(print())`
9. Chain a call to `andExpect(status().isOk())`
10. Chain a call to `andExpect(content().string(containsString("Hello Mock")));`

#### Imports needed

```java
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.containsString;
```

Run the test - it should fail on 1 count - no `GreetingController`.

### The GreetingController

1. Add `GreetingController` to `SmokeTests` and observe a **fail**;
2. Create class `GreetingController` in `main/java/com.example.demo.controllers` with `@RestController` annotation;
3. Re-run tests and check for failure of `GreetingControllerTest` (assertion error Expected 200, Actual 404) but passing `SmokeTests`;
4. Autowire the GreetingService;
5. Provide `@GetMapping("/greeting")` and a method that returns a `String` annotated with `@ResponseBody` called `greeting()`;
6. The method should return the result of calling `greet()` on the `greetingService`
7. Re-run the tests and observe them both passing.

**Take a break!**
