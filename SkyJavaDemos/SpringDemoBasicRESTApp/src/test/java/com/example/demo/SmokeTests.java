package com.example.demo;

import com.example.demo.controllers.DemoController;
import com.example.demo.controllers.GreetingController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
public class SmokeTests {

    @Autowired
    private DemoController demoController;

    @Autowired
    private GreetingController greetingController;

    @Test
    public void contextLoads() {
        // Note that this is from the assertj.core.api.Assertions
        assertThat(demoController).isNotNull();
        assertThat(greetingController).isNotNull();
    }
}
