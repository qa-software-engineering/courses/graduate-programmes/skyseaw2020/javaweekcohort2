package com.example.demo.services;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GreetingServiceTest {

    @Test
    public void callingGreetShouldReturnMessage() {

        GreetingService greetingService = new GreetingService();

        String result = greetingService.greet();

        assertThat(result, is("Well, hello there"));
    }

    @Test
    public void callingGreetMoreShouldReturnAListOf4Strings() {

        GreetingService greetingService = new GreetingService();

        List<String> result = greetingService.greetMore();

        assertThat(result.size(), is(4));

    }
}
