package com.example.demo.services;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GreetingService {

    public String greet() {
        return "Well, hello there";
    }

    public List<String> greetMore() {
        ArrayList<String> greetings = new ArrayList<>();

        greetings.add("Good Morning Vietnam!!!!");
        greetings.add("Hello, good evening and welcome");
        greetings.add("Hello, is it me you're looking for?");
        greetings.add("Hello, I love you won't you tell me your name?");

        return greetings;
    }
}
