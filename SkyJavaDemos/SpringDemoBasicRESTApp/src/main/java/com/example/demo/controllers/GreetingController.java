package com.example.demo.controllers;

import com.example.demo.services.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GreetingController {

    @Autowired
    private GreetingService greetingService;

    @GetMapping("/greeting")
    public @ResponseBody String greeting() {
        return greetingService.greet();
    }

    @GetMapping("/moregreetings")
    public @ResponseBody
    List<String> moregreetings() { return greetingService.greetMore();}
}
