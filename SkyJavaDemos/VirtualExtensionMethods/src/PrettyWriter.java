public interface PrettyWriter extends Writer {

    // Another Interface with a Virtual Extension Method

    @Override
    public default void write(String msg) {
        System.out.println("**** " + msg + " ++++");
    }
}
