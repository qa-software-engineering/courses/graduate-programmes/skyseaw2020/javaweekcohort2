public class Main {

    public static void main(String[] args) {

        // Making Foo

        Foo f = new Foo();
        f.doStuff();

        // Making Bar
        Bar b = new Bar();
        b.doStuff();

        // Mixing types - uses Bar's implementation of writer

        Foo fb = new Bar();
        fb.doStuff();

        System.out.println(fb.getClass());

        // Allowable declarations
        Writer writer1 = new Foo();
        writer1.write("Stuff");
        Writer writer2 = new Bar();
        writer2.write("Stuff");        // PrettyWriter overrides Writer
        //PrettyWriter pw1 = new Foo();     // NO
        PrettyWriter pw2 = new Bar();       // Yes
        pw2.write("Stuff");
    }
}
