public class Car extends Vehicle implements hasEngine, hasPassengers {

    private int power;
    private int wheels = 4;
    private int doors;
    private int numberOfPassengers = 0;
    private String fuelType;
    private int seats;
    protected int passengers = 0;


    public Car(int speed, boolean isCommerical, String colour, String make, String model, int power, String fuelType, int seats) {
        super(speed, isCommerical, colour, make, model);
        this.power = power;
        this.fuelType = fuelType;
        this.seats = seats;
    }

    @Override
    public void accelerate(int timeInSeconds) {
        this.speed = this.speed + (timeInSeconds * power);
    }

    @Override
    public void deccelerate(int timeInSeconds) {
        this.speed = this.speed - (timeInSeconds * power);
    }

    public int getPower() {
        return this.power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getFuelType() {
        return this.fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    @Override
    public void setPassengers(int passengers) {
        if(passengers > seats) {
            passengers = seats;
        }
        else {
            this.passengers = passengers;
        }
    }

    @Override
    public int getPassengers() {
        return passengers;
    }
}
