public interface hasEngine {

    public int getPower();
    public void setPower(int power);

    public String getFuelType();
    public void setFuelType(String fuelType);
}
