public interface hasPayingPassengers {

    public void setFare(int fareRate);
    public int getFare();
}
