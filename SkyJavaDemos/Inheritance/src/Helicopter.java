public class Helicopter extends Vehicle {

    private int propellers;
    private int power;
    private int rotationSpeed;
    private int altitude = 0;


    public Helicopter(int speed, boolean isCommerical, String colour, String make, String model) {
        super(speed, isCommerical, colour, make, model);
    }

    @Override
    public void accelerate(int timeInSeconds) {
        this.speed += (timeInSeconds * ( (power / rotationSpeed)));
    }

    @Override
    public void deccelerate(int timeInSeconds) {
        this.speed -= (timeInSeconds * ( (power / rotationSpeed)));
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }
}
