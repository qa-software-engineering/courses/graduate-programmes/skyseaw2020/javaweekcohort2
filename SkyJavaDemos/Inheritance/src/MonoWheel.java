public class MonoWheel extends Vehicle {

    private int wheels = 1;
    private int power;

    public MonoWheel(int speed, boolean isCommerical, String colour, String make, String model) {
        super(speed, isCommerical, colour, make, model);
    }

    @Override
    public void accelerate(int timeInSeconds) {
        this.speed = this.speed + (timeInSeconds * power);
    }

    @Override
    public void deccelerate(int timeInSeconds) {
        this.speed = this.speed - (timeInSeconds * power);
    }
}
