public abstract class Vehicle {

    protected int speed = 0;
    protected boolean isCommerical;
    protected String colour;
    protected String make;
    protected String model;
//    private String fuelType;
//    private int wheels;


    public Vehicle(int speed, boolean isCommerical, String colour, String make, String model) {
        this.speed = speed;
        this.isCommerical = isCommerical;
        this.colour = colour;
        this.make = make;
        this.model = model;
    }

    public int getSpeed() {
        return speed;
    }

    public boolean isCommerical() {
        return isCommerical;
    }

    public String getColour() {
        return colour;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public void setCommerical(boolean commerical) {
        isCommerical = commerical;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public abstract void accelerate(int timeInSeconds);
    public abstract void deccelerate(int timeInSeconds);

//    public void ascend() {}
//    public void descend() {}
//    public void rotate() {}
//    public void tilt() {}
//    public void fireCannons() {}
//    public void reverse() {}
//    public void useHeadlights() {}
//    public void startEngine() {}
//    public void lowerWheels() {}
//    public void raiseWheels() {}
}
