public class Taxi extends Car implements hasPayingPassengers {
    private int fare = 0;

    public Taxi(int speed, boolean isCommerical, String colour, String make, String model, int power, String fuelType, int seats) {
        super(speed, isCommerical, colour, make, model, power, fuelType, seats);
    }


    @Override
    public void setFare(int fareRate) {
        this.fare = this.passengers * fareRate;
    }

    @Override
    public int getFare() {
        return 0;
    }
}
