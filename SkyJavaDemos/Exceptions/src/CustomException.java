import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CustomException {

    public static void main(String[] args) {

        BufferedReader br = null;

        try{
            br = new BufferedReader(new InputStreamReader(System.in));

            System.out.println("Please enter some text and press Enter");

            String line = br.readLine();

            while (line != null) {
                if(line.equals("no")) {
                    throw new BadLineException("Line said no", line);
                }
                System.out.println(line);
                System.out.println("Please enter some text and press Enter");
                line = br.readLine();
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        catch(BadLineException e) {
            System.out.println(e.getMessage() + ": The line was: " + e.getBadLine());
        }
        finally {
            try {
                if(br != null) br.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("I've executed!");
        }
    }
}
