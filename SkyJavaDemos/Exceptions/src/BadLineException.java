public class BadLineException extends Exception {

    private String line;

    public BadLineException() {
        super("I didn't like the line I read in!");
    }

    public BadLineException(String message, String line) {
        super(message);
        this.line = line;
    }

    public String getBadLine() {
        return line;
    }
}
