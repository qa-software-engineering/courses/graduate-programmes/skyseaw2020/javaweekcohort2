package readers;

import java.io.*;

public class Files1 {

    public static void main(String[] args) throws IOException {

        // BufferedReader, InputStream and FileInputStream

        // FileInputStream handles raw binary data - byte stream class
        // BufferedReader and InputStreamReader belong to character stream category
        // InputStreamReader builds bridge between FileInputStream and BufferedReader

        BufferedReader br = null;
        FileInputStream is = null;

        is = new FileInputStream("src/input.txt");

        br = new BufferedReader(new InputStreamReader(is));

        if(br != null) br.close();
        if(is != null) is.close();
    }
}
