package readers;

import java.io.*;

public class Files3 {

    private BufferedWriter bw = null;
    private BufferedReader br = null;

    public void readFromConsoleAndWriteToFile() throws IOException {

        bw = new BufferedWriter(new FileWriter("src/output.txt"));
        br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Please enter some text and press Enter, type 'stop' and press Enter to quit");

        String line = br.readLine();

        while (!line.equals("stop")) {
            bw.write(line + "\n");          // Writes the text to output.txt
            System.out.println("Please enter some text and press Enter, type 'stop' and press Enter to quit");
            line = br.readLine();
        }
        bw.flush();

        if (br != null) br.close();
        if (bw != null) br.close();

    }
}
