package readers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class Files2 {
    public static void main(String[] args) {

        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader("src/input.txt"));
            String line = br.readLine();

            while (line != null) {
                System.out.println(line);
                line = br.readLine();
            }
        }
        catch(IOException | ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
//        catch(IOException e) {
//            e.printStackTrace();
//        }

        try {
            if (br != null) br.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

}
